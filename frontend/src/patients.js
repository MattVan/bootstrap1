import React, { Component } from 'react';
import { useTranslation, withTranslation, Trans } from 'react-i18next';

const postCodePattern = "^[A-Za-z][0-9][A-Za-z]\\s?[0-9][A-Za-z][0-9]";
//switch error message to indicate format
const zipCodePattern = "^[0-9]{5}(\\-[0-9]{4})?";

const canadaProvList = [
    "British Columbia",
    "Alberta",
    "Saskatchewan",
    "Manitoba",
    "Ontario",
    "Quebec",
    "Newfoundland",
    "P.E.I",
    "Nova Scotia",
    "New Brunswick",
    "Yukon",
    "Northwest Territories",
    "Nunavut"
    ]
  //Thanks Google for this list :D
const usStateList = [
    'Alabama','Alaska','American Samoa','Arizona','Arkansas','California',
    'Colorado','Connecticut','Delaware','District of Columbia',
    'Federated States of Micronesia','Florida','Georgia','Guam','Hawaii',
    'Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine',
    'Marshall Islands','Maryland','Massachusetts','Michigan','Minnesota',
    'Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire',
    'New Jersey','New Mexico','New York','North Carolina','North Dakota',
    'Northern Mariana Islands','Ohio','Oklahoma','Oregon','Palau','Pennsylvania',
    'Puerto Rico','Rhode Island','South Carolina','South Dakota','Tennessee',
    'Texas','Utah','Vermont','Virgin Island','Virginia','Washington',
    'West Virginia','Wisconsin','Wyoming'];

class Patients extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fName:"",
      lName:"",
      emailAdd:"",
      country:"Canada",
      province_state:"British Columbia",
      postCode_zip:"",
      lowBloodPressure:0,
      highBloodPressure:0,
      formClass:"",
      provOrStateList:canadaProvList,
      highBpMessage:"errMsg.highPress",
      highBpClass:"form-control",
      lowBpMessage:"errMsg.lowPress",
      lowBpClass:"form-control",
      emailMsg:"errMsg.email",
      emailClass:"form-control",
      postZipPattern:postCodePattern,
      postZipClass:"form-control"
    };
    
    //Binding all handling functions to update state and will also reset form
    //after a successful submission has been made
    
    this.handleFirstNameChange = this.handleFirstNameChange.bind(this);
    this.handleLastNameChange = this.handleLastNameChange.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handleCountryChange = this.handleCountryChange.bind(this);
    this.handleProvChange = this.handleProvChange.bind(this);
    this.handlePostZipChange = this.handlePostZipChange.bind(this);
    this.handleLowPressChange = this.handleLowPressChange.bind(this);
    this.handleHighPressChange = this.handleHighPressChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  
  handleFirstNameChange(event){
    this.setState({fName:event.target.value});
  }
  
  handleLastNameChange(event){
    this.setState({lName:event.target.value});
  }
  
  handleEmailChange(event){
    this.setState({emailAdd:event.target.value});
  } 
  
  handleCountryChange(event){
    if(event.target.value === "Canada"){
      this.setState({
        country:"Canada", 
        provOrStateList:canadaProvList,
        postZipPattern:postCodePattern,
      });
    } else{
      this.setState({
        country:"U.S.A", 
        provOrStateList:usStateList,
        postZipPattern:zipCodePattern,
      });
    }
  } 
  
  handleProvChange(event){
    this.setState({province_state:event.target.value});
  }
  
  handlePostZipChange(event){
    this.setState({postCode_zip:event.target.value});
  }
  
  handleLowPressChange(event){
    this.setState({lowBloodPressure:event.target.value});
  }
  
  handleHighPressChange(event){
    this.setState({highBloodPressure:event.target.value});
  }
  
  //Handles Creating Patients
  handleSubmit(ev) {
    //Access to patient backend service
    const { patientService } = this.props;
    
    if(ev.target.checkValidity()){
      console.log("125");
      if(parseInt(this.state.lowBloodPressure) > parseInt(this.state.highBloodPressure)){
        this.setState({
          highBpClass:"form-control is-invalid",
          highBpMessage:"errMsg.lowTooHigh",
          lowBpClass:"form-control is-invalid",
          lowBpMessage:"errMsg.lowTooHigh"
        });
      } else {
        console.log("hello 133");
          patientService.create({
            fName:this.state.fName,
            lName:this.state.lName,
            email:this.state.emailAdd,
            country:this.state.country,
            provinceState:this.state.province_state,
            postCode_zip:this.state.postCode_zip,
            lowBloodPressure:this.state.lowBloodPressure,
            highBloodPressure:this.state.highBloodPressure,
          })
          .then(()=>{
            console.log("Created");
            this.setState({
              fName:"",
              lName:"",
              emailAdd:"",
              country:"",
              province_state:"",
              postCode_zip:"",
              lowBloodPressure:0,
              highBloodPressure:0,
              formClass:"",
            });
          })
          .catch(error => {
            console.log("hello 158");
            this.setState({
              formclass: '',
              emailMsg: 'errMsg.email2',
              emailClass: 'form-control is-invalid'
            });
          });
        }
    } else{
      console.log("Hello 167");
      this.setState({
        formClass:"was-validated",
        emailMsg:"errMsg.email",
        emailClass:"form-control",
        highBpClass:"form-control",
        highBpMessage:"errMsg.highPress",
        lowBpClass:"form-control",
        lowBpMessage:"errMsg.lowPress"
      });
    }
    ev.preventDefault();
  }
  
  //Handles Deleting Patients (thats pretty dark...)
  deletePatient(id, ev) {
    
    //fyi: accesses patient backend services
    const { patientService } = this.props;
    
    patientService.remove(id);
    
    ev.preventDefault();
  }

  render() {
    const { t , patients } = this.props;

    return(
    <div>
      <div className="py-5 text-center">
        <h2>Patients</h2>
      </div>

      <div className="row">
        <div className="col-md-12 order-md-1">
          <form onSubmit={this.handleSubmit} className={this.state.formClass} noValidate id="patientform">
            <div className="row">
              
              <div className="col-lg-6 col-md-6 col-sm-12 mb-3">
                <label htmlFor="fName">{t('formLabels.fName')}</label>
                <input type="text" className="form-control" id="fName" maxLength="25"
                  value={this.state.fName} required onChange={this.handleFirstNameChange} />
                <div className="invalid-feedback">
                    {t('errMsg.fName')}
                </div>
              </div>
            
            
              <div className="col-lg-6 col-md-6 col-sm-12 mb-3">
                <label htmlFor="lName">{t('formLabels.lName')}</label>
                <input type="text" className="form-control" id="lName" maxLength="30"
                  value={this.state.lName} required onChange={this.handleLastNameChange} />
                <div className="invalid-feedback">
                    {t('errMsg.lName')}
                </div>
              </div>
              
              <div className="col-lg-6 col-md-6 col-sm-12 mb-3">
                <label htmlFor="email">{t('formLabels.email')}</label>
                <input type="email" className={this.state.emailClass} id="email" maxLength="40"
                  value={this.state.email} required onChange={this.handleEmailChange} />
                <div className="invalid-feedback">
                    {t('errMsg.email')}
                </div>
              </div>
              
              <div className="col-lg-4 col-md-6 mb-3">
                <label htmlFor="make">{t('formLabels.country')}</label>
                <select className="form-control" id="country" required onChange={this.handleCountryChange}>
                  
                  <option value="Canada">Canada</option>
                  <option value="U.S.A">U.S.A</option>
                  
                </select>
                <div className="invalid-feedback">
                    {t('errMsg.country')}
                </div>
              </div>
              
              <div className="col-lg-4 col-md-6 mb-3">
                <label htmlFor="provState">{t('formLabels.provState')}</label>
                  <select className="form-control" id="provState" required onChange={this.handleProvChange}>
                    {this.state.provOrStateList && this.state.provOrStateList.map((area)=>{
                      return <option key={area} value={area}>{area}</option>;
                    })}
                  </select>
                <div className="invalid-feedback">
                    {t('errMsg.provState')}
                </div>
              </div>
              
              <div className="col-lg-4 col-md-6 mb-3">
                <label htmlFor="postOrZip">{t('formLabels.postZip')}</label>
                <input type="text" className={this.state.postZipClass} id="postOrZip" pattern={this.state.postZipPattern}
                  value={this.state.postCode_zip} required onChange={this.handlePostZipChange} />
                <div className="invalid-feedback">
                    {t('errMsg.postZip')}
                </div>
              </div>
    
              <div className="col-lg-4 col-md-6 mb-3">
                <label htmlFor="lowPressure">{t('formLabels.lowPress')}</label>
                <input type="number" className={this.state.lowBpClass} id="lowPressure" min="50" max="225"
                  value={this.state.lowBloodPressure} required onChange={this.handleLowPressChange} />
                <div className="invalid-feedback">
                  {t('errMsg.lowPress')}
                </div>
              </div>

              <div className="col-lg-4 col-md-6 mb-3">
                <label htmlFor="highPressure">{t('formLabels.highPress')}</label>
                <input type="number" className={this.state.highBpClass} id="highPressure" min="50" max="225"
                  value={this.state.highBloodPressure} required onChange={this.handleHighPressChange} />
                <div className="invalid-feedback">
                  {t('errMsg.highPress')}
                </div>
              </div>
            
              <button className="btn btn-primary btn-lg btn-block" type="submit">{t('formLabels.add')}</button>
            </div>
          </form>
        </div>
      </div>
      
      <table className="table">
        <thead>
          <tr>
            <th scope="col">{t('formLabels.fName')}</th>
            <th scope="col">{t('formLabels.lName')}</th>
            <th scope="col">{t('formLabels.email')}</th>
            <th scope="col">{t('formLabels.country')}</th>
            <th scope="col">{t('formLabels.provState')}</th>
            <th scope="col">{t('formLabels.postZip')}</th>
            <th scope="col">{t('table.highPress')}</th>
            <th scope="col">{t('table.lowPress')}</th>
            <th scope="col">{t('table.delete')}</th>
          </tr>
        </thead>
        <tbody>
          {patients && patients.map(patient => <tr key={patient.id}>
            <td>{patient.fName}</td>
            <td>{patient.lName}</td>
            <td>{patient.email}</td>
            <td>{patient.country}</td>
            <td>{patient.provinceState}</td>
            <td>{patient.postCode_zip}</td>
            <td>{patient.highBloodPressure}</td>
            <td>{patient.lowBloodPressure}</td>
            <td><button onClick={this.deletePatient.bind(this, patient.email)} type="button" className="btn btn-danger">{t('table.delete')}</button></td>
          </tr>)}
        </tbody>
      </table>

      <footer className="my-5 pt-5 text-muted text-center text-small">
        <p className="mb-1">&copy; 2020 CPSC 2650</p>
      </footer>
    </div>
    );
  }
}

const PatientsExport = withTranslation()(Patients);

export default PatientsExport;
