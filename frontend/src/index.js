import 'bootstrap/dist/css/bootstrap.min.css';
import $ from 'jquery';
import Popper from 'popper.js';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import React from 'react';
import ReactDOM from 'react-dom';
import ExportedApp from './application';

import './i18n';

ReactDOM.render(
  <ExportedApp />,
  document.getElementById('app')
);
