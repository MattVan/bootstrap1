// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from '@feathersjs/feathers';

export default (options = {}): Hook => {
  return async (context: HookContext) => {
    const { app, data } = context;
    
    //FYI
    console.log(data);
    
    if(!data.fName){
      throw new Error("A first name is required");
    }
    if(!data.lName){
      throw new Error("A last name is required");
    }
    if(!data.email){
      throw new Error("An email name is required");
    }
    if(!data.country){
      throw new Error("A Country name is required");
    }
    if(!data.provinceState){
      throw new Error("A Province/state is required");
    }
    if(!data.postCode_zip){
      throw new Error("A Post or Zip code is required");
    }
    if(!data.lowBloodPressure){
      throw new Error("Low blood pressure is required");
    }
    if(!data.highBloodPressure){
      throw new Error("High blood pressure is required");
    }
    
    if(data.fName.length > 25){
     throw new Error("First name cannot be longer than 20 characters"); 
    }
    
    if(data.lName.length > 30){
      throw new Error("Last name cannot be longer than 20 characters"); 
    }
    
    if(data.email.length > 40){
      throw new Error("Email cannot be longer than 40 characters"); 
    }
    
    const patients = await app.service('patients').find({
      query: {
        email: data.email
      }
    });
    
    if(patients.total>0){
      throw new Error("Email already exists");
    }
    
    if(parseInt(data.highBloodPressure) < parseInt(data.lowBloodPressure)){
      throw new Error("High blood pressure cannot be lower than low blood pressure"); 
    }
  
    return context;
  };
}
